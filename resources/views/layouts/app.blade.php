<!--NOTE: I toke all code related of html from my own projects to save my time -->
{{-- Layout base admin panel --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! HTML::style('css/app.css') !!}

    @yield('head_css')
    {{-- End head css --}}

</head>

    <body>
        {{-- navbar --}}
       

        {{-- content --}}
        <div class="container-fluid">
            @yield('content')
        </div>

        {{-- Start footer scripts --}}
        @yield('before_footer_scripts')

        {!! HTML::script('js/app.js') !!}
        {!! HTML::script('//cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js') !!}
        @yield('footer_scripts')
        {{-- End footer scripts --}}
    </body>
</html>