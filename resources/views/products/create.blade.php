@extends('layouts.app')

@section('title')
    Create Product
@stop
@section('content')
<div class="row">
    {{ Form::open(['url' => '/products', 'id' => 'product']) }}
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-note"></i>
                        Add Product
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="form-body">
                        <div class="alert alert-danger" style="display: none">
                            <button class="close" data-close="alert"></button> 
                        </div>
                        <div class="alert alert-success" style="display: none">
                            <button class="close" data-close="alert"></button> 
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Name <span class="required" aria-required="true">* </span></label>
                            <div class="col-md-9">
                                {{ Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Quantity <span class="required" aria-required="true">* </span></label>
                            <div class="col-md-9">
                                {{ Form::text('quantity', null, ['placeholder' => 'Quantity', 'class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Price <span class="required" aria-required="true">* </span></label>
                            <div class="col-md-9">
                                {{ Form::text('price', null, ['placeholder' => 'Price', 'class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    {{ Form::submit('Submit', ['class' => 'btn green']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}
</div>
@stop
@section('footer_scripts')
<script type="text/javascript">
    jQuery(document).ready(function() 
    {
//        $('#product').submit(function(e) 
//        {
            var form  = $('#product');

            form.validate({
                errorElement: 'span', 
                errorClass: 'help-block help-block-error', 
                focusInvalid: false, 
                ignore: '',  
                rules: {
                    name: {
                        required: true
                    },
                    quantity: {
                        required: true,
                        digits: true
                    },
                    price: {
                        required: true,
                        number: true
                    }
                },

                highlight: function (element) { 
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); 
                },

                unhighlight: function (element) { 
                    $(element)
                        .closest('.form-group').removeClass('has-error'); 
                },

                success: function (label, element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); 
                },

                submitHandler: function (form) {
                    ajax_call($(form));
                }
            });
//        });
    
        function ajax_call(form)  
        {
            var error   = $('.alert-danger', form);
            var success = $('.alert-success', form);

            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize(),
                dataType: 'json',
                success: function(data)
                {
                    success.show();
                    error.hide();
                    success.html(data.message);
                },
                error : function(data) 
                { 
                    success.hide();
                    error.show();
                    error.html(data.responseText);
                }
            });
            return false;
        }
    });
</script>
@stop
