<div class="row">
    {{ Form::model($branch, [
        'method' => 'PATCH',
        'url' => ['/branches', $branch->id],
        'id' => 'branch', 'class' => 'form-horizontal'
    ]) }}
        <div class="col-md-6">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-note"></i>
                        Edit Branch
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="reload" id="reload" data-original-title title></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Code <span class="required" aria-required="true">* </span></label>
                            <div class="col-md-9">
                                {{ Form::text('code', null, ['placeholder' => 'Code', 'class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Name <span class="required" aria-required="true">* </span></label>
                            <div class="col-md-9">
                                {{ Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Client Bank</label>
                            <div class="col-md-9">
                                {{ getBank(1, 'client_bank_id') }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Office Bank</label>
                            <div class="col-md-9">
                                {{ getBank(2, 'office_bank_id') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-note"></i>
                        Contact Details
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Work</label>
                            <div class="col-md-9">
                                {{ Form::text('work_phone_no', null, ['placeholder' => 'Work', 'class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Fax</label>
                            <div class="col-md-9">
                                {{ Form::text('fax_no', null, ['placeholder' => 'Fax', 'class' => 'form-control']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-note"></i>
                        Auto Bill Numbering
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Auto Number Bills</label>
                            <div class="col-md-8">
                                <div class="mt-checkbox-inline">
                                    <label class="mt-checkbox mt-checkbox-outline">
                                        {{ Form::checkbox('auto_number_bills', 1) }}
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Next Number</label>
                            <div class="col-md-8">
                                {{ Form::text('next_number', null, ['placeholder' => 'Next Number', 'class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Reset to Number</label>
                            <div class="col-md-8">
                                {{ Form::text('reset_to_number', null, ['placeholder' => 'Reset to Number', 'class' => 'form-control']) }}
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <hr>
                            <div class="col-md-offset-4 col-md-8">
                                {{ Form::submit('Submit', ['class' => 'btn green']) }}
                                {{ Form::button('Cancel', ['id' => 'cancel', 'class' => 'btn default']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-note"></i>
                        Address
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="form-body" style="height: 714px">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-icon right">
                                    <i class="fa fa-info-circle tooltips" data-original-title="Address Line1 (Max Length is 200 Characters)" data-container="body"></i>
                                    {{ Form::text('address_line_1', null, ['placeholder' => 'Address Line1', 'class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-icon right">
                                    <i class="fa fa-info-circle tooltips" data-original-title="Address Line2 (Max Length is 200 Characters)" data-container="body"></i>
                                    {{ Form::text('address_line_2', null, ['placeholder' => 'Address Line2', 'class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-icon right">
                                    <i class="fa fa-info-circle tooltips" data-original-title="Address Line3 (Max Length is 200 Characters)" data-container="body"></i>
                                    {{ Form::text('address_line_3', null, ['placeholder' => 'Address Line3', 'class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Town</label>
                            <div class="col-md-9">
                                <div class="input-icon right">
                                    <i class="fa fa-info-circle tooltips" data-original-title="Town (Max Length is 200 Characters)" data-container="body"></i>
                                    {{ Form::text('town', null, ['placeholder' => 'Town', 'class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">County</label>
                            <div class="col-md-9">
                                <div class="input-icon right">
                                    <i class="fa fa-info-circle tooltips" data-original-title="County (Max Length is 200 Characters)" data-container="body"></i>
                                    {{ Form::text('county', null, ['placeholder' => 'County', 'class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Post Code</label>
                            <div class="col-md-9">
                                <div class="input-icon right">
                                    <i class="fa fa-info-circle tooltips" data-original-title="Post Code (Max Length is 15 Characters)" data-container="body"></i>
                                    {{ Form::text('post_code', null, ['placeholder' => 'Post Code', 'class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Country</label>
                            <div class="col-md-9">
                                {{ getCountries('country_id', null) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Has DX</label>
                            <div class="col-md-9">
                                <div class="mt-checkbox-inline">
                                    <label class="mt-checkbox mt-checkbox-outline">
                                        {{ Form::checkbox('has_dx', 1, null, ['id' => 'has_dx']) }}
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group number" style="display: none">
                            <label class="col-md-3 control-label">Number</label>
                            <div class="col-md-9">
                                {{ Form::text('number', null, ['placeholder' => 'Number', 'class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group location" style="display: none">
                            <label class="col-md-3 control-label">Location</label>
                            <div class="col-md-9">
                                {{ Form::text('location', null, ['placeholder' => 'Location', 'class' => 'form-control']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}
</div>
<script type="text/javascript">
    jQuery(document).ready(function() 
    {
        legal.init();
        $('#reload').click(function()
        {
            var id = '{{$branch->id}}';
            var url = base_url +'/branches/'+ id +'/edit';
            return_page(url);
            return false;
        });
//------------------------------------Cancel------------------------------------

        $('#cancel').click(function()
        {
            var url = base_url +'/branches';
            return_page(url);
            return false;
        });
        
//-------------------------Number & Location Show/Hide--------------------------

        $('#has_dx').click (function ()
        {
            if ($(this).is(':checked')) 
            {
                $('.number').show();
                $('.location').show();
            }else
            {
                $('.number').hide();
                $('.location').hide();
            }
        });
        
        if ($('#has_dx').is(':checked')) 
        {
            $('.number').show();
            $('.location').show();
        }else
        {
            $('.number').hide();
            $('.location').hide();
        }
    });
</script>
