@extends('layouts.app')

@section('title')
    Products List
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings"></i>
                    Products
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="{{ url('/products/create') }}" class="btn sbold green ajaxify"> 
                                    Add New <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover datatable">
                    <thead> 
                        <tr>
                            <th>Product name</th>
                            <th>Quantity in stock</th>
                            <th>Price per item</th>
                            <th>Datetime submitted</th>
                            <th>Total value number</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $totalPrice = 0.00;?>
                        @if(count($jsonData) > 0)
                            @foreach($jsonData as $data)
                                <?php $quantity = $data['quantity'];
                                $price = $data['price'];
                                $totalValue = $quantity * $price;
                                $totalPrice = $totalPrice + $totalValue;?>
                                <tr>
                                    <td>{{ $data['name'] }}</td>
                                    <td>{{ $quantity }}</td>
                                    <td>{{ $price }}</td>
                                    <td>{{ $data['datetime'] }}</td>
                                    <td>{{ $totalValue }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="4">Total:</td>
                                <td>{{ $totalPrice }}</td>
                            </tr>
                        @else 
                            <tr><td colspan="5">No Record Found</td></tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
@section('footer_scripts')
<script type="text/javascript">
    jQuery(document).ready(function() 
    {

    });
</script>
@stop
