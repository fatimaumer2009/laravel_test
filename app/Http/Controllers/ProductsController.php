<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $data = array();
        
        $fileName = public_path().'/json/products.json';
        
        if(\File::exists($fileName))
        {
            $jsonOldData = file_get_contents($fileName);

            $jsonData = json_decode($jsonOldData, true);
        }
        
        return view('products.index', compact('jsonData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $oldData = array();
        
        $this->validate($request, [
            'name'     => 'required',
            'quantity' => 'required|digits_between:1,20',
            'price'    => 'required|numeric'
        ]);
        
        $fileName = public_path().'/json/products.json';
        
        $data['name']     = $request['name'];
        $data['price']    = $request['price'];
        $data['quantity'] = $request['quantity'];
        $data['datetime'] = date('Y-m-d H:i:s');
        
        if(\File::exists($fileName))
        {
            $jsonOldData = file_get_contents($fileName);

            $oldData = json_decode($jsonOldData, true);
        }
        
        array_push($oldData, $data);
        
        $jsonData = json_encode($oldData);

        file_put_contents($fileName, $jsonData);

        return response()->json(['response' => 'success', 'message' => 'Product added!']);
    }
}
